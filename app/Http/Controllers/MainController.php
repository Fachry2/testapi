<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Helpers\Action;

use DB;

class MainController extends Controller
{   
    public function __construct(){}

    function login(Request $request)
    {
        $username = $request->json('username');
        $password = $request->json('password');

        $check = DB::table('users')
            ->where('username', $username)
            ->orWhere('email', $username)
            ->first();
        $status = 'Authentication failed';
        $results = 400;

        if ($check != NULL) {
            $decrypt_password = Crypt::decrypt($check->password);
            if ($password == $decrypt_password) {
                $status = $check->secret_key;
                $results = 200;
            }
        } 

        return response()->json($status, $results);
    }

    public function list()
    {
        $res=[
              "meta"=> [
                "count"=> 10,
                "total"=> 100
              
              ],
              "links"=> [
                "first"=> "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=0",
                "last"=> "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
                "next"=> "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
                "prev"=> "null"
              ],
              "data"=> [
                [
                  "name"=> "foo template",
                  "checklist"=> [
                    "description"=> "my checklist",
                    "due_interval"=> 3,
                    "due_unit"=> "hour"
                  ],
                  "items"=> [
                    [
                      "description"=> "my foo item",
                      "urgency"=> 2,
                      "due_interval"=> 40,
                      "due_unit"=> "minute"
                    ],
                    [
                      "description"=> "my bar item",
                      "urgency"=> 3,
                      "due_interval"=> 30,
                      "due_unit"=> "minute"
                    ]
                  ]
                ]
              ]];
        return response()->json($res);
    }
}
