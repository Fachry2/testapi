<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class BasicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorization=$request->header('authorization');

        $check = DB::table('users')
            ->where('secret_key', $authorization)
            ->first();
        if ($check) {
            return $next($request);
        }else{
            return response()->json(["Error"=>"Need authorization"],500);
        }
    }
}
