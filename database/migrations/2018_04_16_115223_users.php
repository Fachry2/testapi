<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50)->unique();
            $table->string('email', 50)->unique();
            $table->string('full_name', 255);
            $table->string('password', 255);
            $table->string('secret_key', 255);
            $table->string('forgot_password', 255)->nullable();
            $table->string('avatar', 255)->nullable();
            $table->string('phone', 15)->nullable();
            $table->longText('address', 255)->nullable();
            $table->boolean('gender')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
