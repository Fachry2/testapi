<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'full_name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => 'eyJpdiI6Imp0eVdRaFFpam1DWGhQSEdzYlMxanc9PSIsInZhbHVlIjoieXNVSjQxdm9yY1VyUHlrY2tmU0ZGdz09IiwibWFjIjoiNjc3MTI5YmFkMDhmMDYzNGIxNWU1MTViZTdmNjdjOWRlMjJhYWEwYTM4NjcyY2I3Mzg1NGIyZGEyZDJiZTliZCJ9',
            'secret_key' => 'eyJpdiI6IndoSk9QTGR4R1FvemMrNnF1cEhNQkE9PSIsInZhbHVlIjoiV3Z5bUZiUVowM09JXC8zTzdqa203TGc9PSIsIm1hYyI6IjQ1ODE3MTY4OWVkZTE4ZDAwMzExMzdjMjE1MGFmNDdkNDdjM2FiOTU4MmI2NzIwYjc5YjEwNzEyY2JhZmIyYmQifQ==',
        ]);
    }
}
