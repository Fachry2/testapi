# Dokumentasi API CCPOS

## Login
`/login` **[POST]**  
Return header key
Request post yang dibutuhkan:
- username
- password

## Staff
`/get/staff/{limit}/{key}` **[GET]**  
Return daftar staff 
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/staff/search/{keywords}/{limit}/{key}` **[GET]**  
Return search staff 
- {keywords} keyword pencarian berdasarkan nama staff 
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/post/add/staff/{key}` **[POST]**  
Menambahkan staff  
Request post yang dibutuhkan:
- full_name
- username
- password
- email
- password
- role

`/post/update/staff/{id}/{key}` **[POST]**  
Mengubah data staff  
- {id} staff_id

Request post yang dibutuhkan:
- full_name
- username
- password
- email
- password
- role

`/remove/staff/{id}/{key}` **[DELETE]**  
Menghapus data staff 
- {id} staff_id

## Pengaturan Role
`/get/role/{limit}/{key}` **[GET]**
Return daftar role
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/role/search/{keywords}/{limit}/{key}` **[GET]**
Return daftar role sesuai keywords
- {keywords} name role
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/post/role/insert/{key}` **[POST]**
Menambahkan role pengguna
Request yang dibutuhkan:
- name
- level_access (r, rw, atau rwx)
- cashier (0 atau 1)
- list_transaction (0 atau 1)
- inventory (0 atau 1)
- product (0 atau 1)
- table (0 atau 1)
- report (0 atau 1)
- settings (0 atau 1)

`/post/role/update/{id}/{key}` **[POST]**
Mengubah data role pengguna
Request yang dibutuhkan:
- name
- level_access (r, rw, atau rwx)
- cashier (0 atau 1)
- list_transaction (0 atau 1)
- inventory (0 atau 1)
- product (0 atau 1)
- table (0 atau 1)
- report (0 atau 1)
- settings (0 atau 1)

`/remove/role/{id}/{key}` **[DELETE]**
Menghapus data role
- {id} id role

## Membership
`/get/membership/{limit}/{key}` **[GET]**  
Return data membership  

`/post/add/membership` **[POST]**  
Daftar membership  
Request post yang dibutuhkan:
- full_name
- username
- email
- password
- avatar
- gender
- photo
- address
- phone

`/post/update/membership/{id}` **[POST]**  
Update data membership  
- {id} membership_id

Request post yang dibutuhkan:
- full_name
- username
- email
- password
- address
- avatar
- gender
- phone

`/get/detail/membership/{key}` **[GET]**  
Return detail profil membership berdasarkan key login membership

## Tax
`/get/tax/{limit}/{key}` **[GET]**  
Return tax settings
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/post/tax/insert/{key}` **[POST]**  
Menambahkan tax settings  

`/post/tax/update/{id}/{key}` **[POST]**  
Mengubah data tax settings
- {id} tax_id

`/remove/tax/{id}/{key}` **[DELETE]**  
Menghapus data tax settings
- {id} tax_id

## Inventory  
`/get/inventory/{limit}/{key}` **[GET]**  
Return inventory  
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/inventory/search/{keywords}/{limit}/{key}` **[GET]**  
Return search inventory
- {keywords} keyword pencarian berdasarkan nama inventory
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/view/inventory/{id}/{key}` **[GET]**  
Return detail inventory
- {id} inventory_id

`/post/insert/inventory/{key}` **[POST]**  
Menambahan inventory  
Request post yang dibutuhkan:
- name
- quantity
- size

`/post/update/inventory/{id}/{key}` **[POST]**  
Mengubah data inventory  
- {id} inventory_id  

Request post yang dibutuhkan:
- name
- quantity
- size

`/remove/inventory/{id}/{key}` **[DELETE]**  
Menghapus data inventory  
- {id} inventory_id 

`/get/history-inventory/{limit}/{key}` **[GET]**  
Return history inventory  
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/post/increase/inventory/{id}/{key}` **[POST]**  
Menambah stok inventory 
- {id} inventory_id 

Request post yang dibutuhkan:
- quantity
- size

## Library 
`/get/library/{limit}/{key}` **[GET]**  
Return library 
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada limit
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/library/search/{keywords}/{limit}/{key}` **[GET]**  
Return search library 
- {keywords} keyword pencarian berdasarkan nama library 
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada limit
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/library/search-by-category/{keywords}/{id}/{limit}/{key}` **[GET]**  
Return search library berdasarkan kategori
- {keywords} keyword pencarian berdasarkan nama library
- {id} id kategori
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada limit
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/post/library/insert/{key}` **[POST]**  
Menambahkan library 
Request post yang dibutuhkan: 
- category_id
- code
- name
- price
- reciepe_item
- reciepe_quantity
- reciepe_size
- photo

`/post/library/update/{id}/{key}` **[POST]**  
Mengubah data library 
- {id} library_id
Request post yang dibutuhkan:
- category_id
- code
- name
- price
- reciepe_item
- reciepe_quantity
- reciepe_size
- photo 

`/remove/library/{id}/{key}` **[DELETE]**  
Menghapus data library
- {id} library_id

## Library Packet
`/get/library/packet/{limit}/{key}` **[GET]**  
Return library packet  
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada limit
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data  

`/get/library/packet/search/{keywords}/{limit}/{key}` **[GET]**  
Return search library packet 
- {keywords} keyword pencarian berdasarkan nama packet 
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada limit
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data  

`/get/library/packet/view/{id}/{key}` **[GET]**  
Return detail library packet 
- {id} library_packet_id 

`/post/library/packet/insert/{key}` **[POST]**  
Menambahan library packet 
Request post yang dibutuhkan: 
- code
- name
- library_id
- library_quantity
- photo
- price 

`/post/library/packet/update/{id}/{key}` **[POST]**  
Mengubah data library packet 
- {id} library_packet_id   

Request post yang dibutuhkan:
- code
- name
- library_id
- library_quantity
- photo
- price 

`/remove/library/packet/{id}/{key}` **[DELETE]**  
Menghapus data library packet 
- {id} library_packet_id

## Library Category
`/get/library/category/{limit}/{key}` **[GET]**  
Return library category 
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan semua data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/library/category/search/{keywords}/{limit}/{key}` **[GET]**  
Return search library category
- {keywords} keyword pencarian berdasarkan nama category
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan semua data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`/get/library/category/view/{id}/{limit}/{key}` **[GET]**  
Return semua library dengan library_category_id yang sama 
- {id} library_category_id
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data  

`/post/library/category/insert/{key}` **[POST]**  
Menambahkan data library category  
Request post yang dibutuhkan:
- code
- name

`/post/library/category/update/{id}/{key}` **[POST]**  
Mengubah data library category  
- {id} library_category_id
Request post yang dibutuhan: 
- code
- name

## Meja
`get/table/{limit}/{key}` **[GET]**
Return daftar meja
- {limit} 'all' mengembalikan total jumlah data
- {limit} 'cashier' mengembalikan tanpa ada pagging
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`get/table/search/{keywords}/{limit}/{key}` **[GET]**
Return pencarian daftar meja
- {search} keyword nomor meja
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`get/table/by/{status}/{limit}/{key}` **[GET]**
Return daftar meja berdasarkan status
- {status} jenis status 'terisi' atau 'kosong'
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data 

`get/table/view/{id}/{key}` **[GET]**
Return detil table berdasarkan id meja
- {id} id meja

`post/table/insert/{key}` **[GET]**
Menambahkan meja
Request yang dibutuhkan:
- table_number (Nomor meja)
- chairs_amount (Jumlah kursi)

`post/table/update/{id}/{key}` **[GET]**
Mengubah data meja
Request yang dibutuhkan
- table_number (Nomor meja)
- chairs_amount (Jumlah kursi)

`remove/table/{id}/{key}` **[GET]**
Menghapus meja
- {id} id meja

## Cashier && Waitress
`/get/purchased/{limit}/{key}` **[GET]**  
Return purchased 
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data  

`/get/purchased/by/{status}/{limit}/{key}` **[GET]**    
Return purchased order
- {limit} 'all' mengembalikan total jumlah data
- {limit} number bisa digunakan juga untuk pagging, berdasarkan limit data sebesar 50 data  

`/get/purchased/with/table/{key}` **[GET]**    
Return meja bersamaan dengan purchased

`/post/purchased/create/{key}` **[POST]**    
Menambahkan order baru. Return purchased_id
Request post yang dibutuhkan:
- table_number ( Nomor Meja )
- visitor_name ( Nama pengunjung )
- number_of_visitor ( Jumlah pengunjung )

`/get/purchased/total/{id}/{key}` **[GET]**    
Return total pembayaran order
- {id} purchased_id 

`/get/purchased/item/{id}/{key}` **[GET]**    
Return daftar item pada purchased order
- {id} purchased_id 

`/get/purchased/item/view/{id}/{key}` **[GET]**    
Return detil purchased item
- {id} library_id 

`/post/purchased/item/insert/{id}/{key}` **[POST]**    
Menambahkan item ke purchased order 
- {id} purchased_id  

Request post yang dibutuhkan:
- library_id ( ID library ) Dapat dikosongkan jika memilih packet
- packet_id ( ID Packet ) Dapat dikosongkan jika memilih library 
- quantity ( Kuantitas pesanan )
- price ( Harga didapatkan dari library )
- note
 
`/post/purchased/item/update/note/{id}/{key}` **[POST]**

Mengubah note pesanan
- {id} purchased_id

Request post yang dibutuhkan
- note
- id library

`/put/purchased/item/increase/{id}/{key}` **[PUT]**    
Menambahkan kuantitas item yang dibeli 
- {id} purchased_item_id 

`/put/purchased/item/decrease/{id}/{key}` **[PUT]**  
Mengurangkan kuantitas item yang dibeli 
- {id} purchased_item_id 

`purchased/complete/{id}/{key}` **[PUT]**
Menyelesaikan pembayaran
- {id} purchased_id

`/remove/purchased/item/{id}/{key}` **[DELETE]**    
Menghapus purchased item 
- {id} purchased_item_id

`/purchased/print/{id}/{key}` **[GET]**
Mencetak struk pembayaran
- {id} id purchased

`purchased/print/kitchen/{id}/{key}` **[GET]**
Mencetak struk untuk dapur
- {id} id purchased
