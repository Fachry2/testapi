<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        .page-break {
            page-break-after: always;
        }
        html {
            padding: 0;
        }
        body {
            width: 100%;
            float: left;
            font-family: Arial;
            padding: 0;
            margin: 0;
            font-size: 14px;
        }
        p {
            margin: 0;
        }
        .header {
          text-align: center;
        }
        .full-border {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
            margin-bottom: 100px;
            font-size: 13px;
        }
        .full-border th {
            font-weight: 600;
            text-align: center;
        }
        .full-border td, .full-border th {
            border: 1px solid #000;
            padding: 5px;
        }
        .no-border {
            width: 40%;
            float: right;
        }
        .no-border th {
            padding: 5px;
            padding-right: 30px;
        }
        .no-border td, {
            padding: 5px;
            text-align: right;
        }
        </style>
    </head>
    <body>
        <h3 style="text-align: center;"><?= $title ?></h3>
        <table class="full-border">
        <?php 
        $ppn = 0;
        $tax = 0;
        $discount = 0;
        $total = 0;
        for ($i = 0; $i < count($report); $i++): ?>
            <tr>
                <th style="background-color: #5994f2; color: #FFF;">NO ORDER : </th>
                <th colspan="4" style="text-align: left; background-color: #5994f2; color: #FFF"><?= sprintf('%04d', $report[$i]->id) ?></th>
            </tr>
            <tr>
                <th>WAKTU DATANG</th>
                <th>WAKTU BAYAR</th>
                <th>KASIR</th>
                <th>PELAYAN</th>
                <th>JENIS PEMBAYARAN</th>
            </tr>
            <tr>
                <td><?= $report[$i]->start_date ?></td>
                <td><?= $report[$i]->end_date ?></td>
                <td style="text-align: center;"><?= $report[$i]->staff ?></td>
                <td style="text-align: center;">
                    <?php if (array_key_exists('waitress', $report[$i])): ?>
                        <?= $report[$i]->waitress ?>
                    <?php endif; ?>
                </td>
                <td style="text-align: center;"><?= $report[$i]->payment_type_name ?></td>
            </tr>
            <tr>
                <th colspan="2">NAMA PRODUK</th>
                <th>QTY</th>
                <th>@</th>
                <th>TOTAL</th>
            </tr>
            <?php for ($j = 0; $j < count($report[$i]->purchased_item); $j++): ?>
                <tr>
                    <td colspan="2"><?= $report[$i]->purchased_item[$j]->name ?></td>
                    <td style="text-align: center;"><?= $report[$i]->purchased_item[$j]->quantity ?></td>
                    <td style="text-align: right;"><?= number_format($report[$i]->purchased_item[$j]->price, 0, '', ',') ?></td>
                    <td style="text-align: right;"><?= number_format($report[$i]->purchased_item[$j]->total, 0, '', ',') ?></td>
                </tr>
            <?php endfor; ?>
            <tr>
                <td colspan="4" style="text-align: left; font-weight: bold;">SUBTOTAL</td>
                <td style="text-align: right; font-weight: bold;"><?= number_format($report[$i]->sub_total, 0, '', ',') ?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: left; font-weight: bold;">PPN</td>
                <td style="text-align: right; font-weight: bold;"><?= number_format($report[$i]->ppn, 0, '', ',') ?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: left; font-weight: bold;">TAX</td>
                <td style="text-align: right; font-weight: bold;"><?= number_format($report[$i]->tax_service, 0, '', ',') ?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: left; font-weight: bold;">DISC</td>
                <td style="text-align: right; font-weight: bold;"><?= number_format($report[$i]->discount_total, 0, '', ',') ?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center; font-weight: bold; background-color: #d2d5db;">TOTAL PEMBAYARAN</td>
                <td style="text-align: right; font-weight: bold; background-color: #d2d5db;"><?= number_format($report[$i]->bill_rounding, 0, '', ',') ?></td>
            </tr>
        <?php 
        $ppn += $report[$i]->ppn;
        $tax += $report[$i]->tax_service;
        $discount += $report[$i]->discount_total;
        endfor; ?>
        <?php for ($i = 0; $i < count($payment); $i++): ?>
            <tr>
                <th colspan="4" style="text-align: left;"><?= $payment[$i]['name'] ?></th>
                <td style="font-weight: bold;">
                    <p style="text-align: left;">Rp</p>
                    <p style="margin-top: -15px; text-align: right;"><?= number_format($payment[$i]['total'], 0, '', ',') ?></p>
                </td>
            </tr>
        <?php 
        $total += $payment[$i]['total'];
        endfor; ?>
        <tr>
            <th style="text-align: center;">PPN</th>
            <th style="text-align: center;">TAX</th>
            <th style="text-align: center;">DISCOUNT</th>
            <th colspan="2" style="text-align: center;">TOTAL</th>
        </tr>
        <tr>
            <td style="text-align: center; background-color: #d2d5db; font-weight: bold;"><?= number_format($ppn, 0, '', ',') ?></td>
            <td style="text-align: center; background-color: #d2d5db; font-weight: bold;"><?= number_format($tax, 0, '', ',') ?></td>
            <td style="text-align: center; color: red; background-color: #d2d5db; font-weight: bold;"><?= number_format($discount, 0, '', ',') ?></td>
            <td colspan="2" style="text-align: center; background-color: #d2d5db; font-weight: bold;"><?= number_format($total, 0, '', ',') ?></td>
        </tr>
        </table>
    </body>
</html>