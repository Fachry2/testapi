<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        .page-break {
            page-break-after: always;
        }
        html {
            padding: 0;
        }
        body {
            width: 100%;
            float: left;
            font-family: Arial;
            padding: 0;
            margin: 0;
            font-size: 14px;
        }
        p {
            margin: 0;
        }
        .header {
          text-align: center;
        }
        .full-border {
            width: 100%;
            border: 1px solid #000;
            border-collapse: collapse;
            margin-bottom: 100px;
            font-size: 13px;
        }
        .full-border th {
            font-weight: 600;
            text-align: center;
        }
        .full-border td, .full-border th {
            border: 1px solid #000;
            padding: 5px;
        }
        .no-border {
            width: 40%;
            float: right;
        }
        .no-border th {
            padding: 5px;
            padding-right: 30px;
        }
        .no-border td, {
            padding: 5px;
            text-align: right;
        }
        </style>
    </head>
    <body>
        <h3 style="text-align: center;"><?= $title ?></h3>
        <table class="full-border">
            <thead>
                <tr>
                    <th>Tgl</th>
                    <th>Penjualan</th>
                    <th>Discount</th>
                    <th>Tax 10%</th>
                    <th>Service Charge 5%</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total_purchased = 0;
                $diskon = 0;
                $ppn = 0;
                $tax_service = 0;
                $value = 0;
                for ($i = 0; $i < count($report); $i++): ?>
                    <tr>
                        <td style="text-align: center;"><?= $report[$i]['name'] ?></td>
                        <td>
                            <p>Rp</p>
                            <p style="text-align: right; margin-top: -31px;"><?= number_format($report[$i]['purchased'], 0, '', ',') ?></p>
                        </td>
                        <td>
                            <p>Rp</p>
                            <p style="text-align: right; margin-top: -31px;"><?= number_format($report[$i]['diskon'], 0, '', ',') ?></p>
                        </td>
                        <td>
                            <p>Rp</p>
                            <p style="text-align: right; margin-top: -31px;"><?= number_format($report[$i]['ppn'], 0, '', ',') ?></p>
                        </td>
                        <td>
                            <p>Rp</p>
                            <p style="text-align: right; margin-top: -31px;"><?= number_format($report[$i]['tax_service'], 0, '', ',') ?></p>
                        </td>
                        <td style="font-weight: bold;">
                            <p>Rp</p>
                            <p style="text-align: right; margin-top: -31px;"><?= number_format($report[$i]['value'], 0, '', ',') ?></p>
                        </td>
                    </tr>
                <?php 
                $total_purchased += $report[$i]['purchased'];
                $diskon += $report[$i]['diskon'];
                $ppn += $report[$i]['diskon'];
                $tax_service += $report[$i]['tax_service'];
                $value += $report[$i]['value'];
                endfor; ?>
                <tr>
                    <td style="font-weight: bold; text-align: center;">TOTAL</td>
                    <td style="font-weight: bold;">
                        <p>Rp</p>
                        <p style="text-align: right; margin-top: -31px;"><?= number_format($total_purchased, 0, '', ',') ?></p>
                    </td>
                    <td></td>
                    <td style="font-weight: bold;">
                        <p>Rp</p>
                        <p style="text-align: right; margin-top: -31px;"><?= number_format($ppn, 0, '', ',') ?></p>
                    </td>
                    <td style="font-weight: bold;">
                        <p>Rp</p>
                        <p style="text-align: right; margin-top: -31px;"><?= number_format($tax_service, 0, '', ',') ?></p>
                    </td>
                    <td style="font-weight: bold;">
                        <p>Rp</p>
                        <p style="text-align: right; margin-top: -31px;"><?= number_format($value, 0, '', ',') ?></p>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="no-border">
        <?php for ($i = 0; $i < count($payment); $i++): ?>
            <tr>
                <th><?= $payment[$i]['name'] ?></th>
                <td style="font-weight: bold;">
                    <p style="text-align: left;">Rp</p>
                    <p style="margin-top: -15px;"><?= number_format($payment[$i]['total'], 0, '', ',') ?></p>
                </td>
            </tr>
        <?php endfor; ?>
        </table>
    </body>
</html>