<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Index Home';
});

Route::post('/login', 'MainController@login');

Route::group(['prefix'=>"checklists/templates",'middleware'=>'basic'],function () {
	Route::get('/', 'MainController@list');
});
